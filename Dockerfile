FROM python:3.7

COPY . .
RUN pip install -r requirements.txt
EXPOSE 8080
ENTRYPOINT FLASK_APP=flaskController.py flask run --host=0.0.0.0 --port=8080