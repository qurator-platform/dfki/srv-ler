#!/usr/bin/python
# This scripts loads a pretrained model and a raw .txt files. It then performs sentence splitting and tokenization and passes
# the input sentences to the model for tagging. Prints the tokens and the tags in a CoNLL format to stdout
# Usage: python RunModel.py modelPath inputPath
# For pretrained models see docs/Pretrained_Models.md

from __future__ import print_function
from somajo import Tokenizer, SentenceSplitter
from pynif import NIFCollection
from util.preprocessing import addCharInformation, createMatrices, addCasingInformation
from neuralnets.BiLSTM import BiLSTM
import sys
import tensorflow as tf
import re
from nif.annotation import *


graph = tf.get_default_graph()


# :: Tokenize German text ::
def SentenceSplit(text):

    tokenizer = Tokenizer(split_camel_case=False, token_classes=False, extra_info=False)
    tokens = tokenizer.tokenize(text)

    sentence_splitter = SentenceSplitter(is_tuple=False)
    sentences = sentence_splitter.split(text)

    sentences = [{'text': sent,'tokens': tokenizer.tokenize(sent)} for sent in sentences]
    #sentences.append({'tokens': sent,'text':text})
    #print(sentences)
    return sentences

# :: Read the input ::
def ReadNIF(inputPath):
    with open(inputPath, 'r', encoding='utf8') as f: text = f.read()
    parsed_collection = NIFCollection.loads(text, format='turtle')
    if len(parsed_collection.contexts) == True:
        sentences = SentenceSplit(parsed_collection.contexts[0].mention)
        sentences = [{'tokens': sent} for sent in sentences]
        return sentences
    else: 
        print("Error: More mentions in NIF than one ")
        return []

def ReadTXT(inputPath):
    with open(inputPath, 'r', encoding='utf8') as f: text = f.read()
    sentences = SentenceSplit(text)
    sentences = [{'tokens': sent} for sent in sentences]
    return sentences
    

def run(inpt): 

    with graph.as_default():
    
      output = ""
      txt = "Am 7. Maerz 2006 traf sich die saarlaendische Landesregierung unter Vorsitz des Ministerpraesidenten Mueller mit Vertretern der Evangelischen Kirche."  
      # input some sentence
      if len(inpt) > 0:
          txt = inpt   
 
      sentences = SentenceSplit(txt)
      #sentences = [{'tokens': sent} for sent in sentences]
      tokenoffsets = []
      offset = 0
      # sentences = sent_tokenize(isstr) # make sure that this is in line with whatever the backend (ner.py in my original case) is doing to process the entire text (i.e. this may use another procedure for sentence splitting or tokenisation)

      #for si, sentence in enumerate(sentences):
      for sentenceIdx in range(len(sentences)):
          print(sentences[sentenceIdx])
          tokens = sentences[sentenceIdx]['tokens']
          sentence = sentences[sentenceIdx]['text']
          #tokens = word_tokenize(sentence)
          tls = len(tokens)
          for ti, token in enumerate(tokens):
              if ti == 0 and not sentence.startswith(token): # this is to check for initial whitespaces
                  offset += sentence.find(token)
              tl = len(token)
              start, end = offset, offset+tl
              #checkOffsets(txt, token, start, end) # in place method to check and crash when indexing goes off (may want to comment this out when I'm positive this works in all cases)
              tokenoffsets.append((start, end))
              offset += tl
              if ti < tls-1:
                  # getting space in between current and next token here
                  offset += txt[end:].find(tokens[ti+1])

          if sentenceIdx < len(sentences)-1:
              # getting length of trailing stuff here
              offset += txt[offset:].find(sentences[sentenceIdx+1]['text'])

      # :: Load the model ::
      lstmModel = BiLSTM.loadModel("models/blstm-cnn-crf.h5")
  
      # :: Read input ::
      #sentences = [{'tokens': sent} for sent in sentences]
      addCharInformation(sentences)
      addCasingInformation(sentences)
      dataMatrix = createMatrices(sentences, lstmModel.mappings, True)
  
      # :: Tag the input ::
      tags = lstmModel.tagSentences(dataMatrix)
 
      tokenCounter = 0
 
      # :: Output to stdout ::
      for sentenceIdx in range(len(sentences)):
          tokens = sentences[sentenceIdx]['tokens']
  
          for tokenIdx in range(len(tokens)):
              tokenTags = []
              for modelName in sorted(tags.keys()):
                  tokenTags.append(tags[modelName][sentenceIdx][tokenIdx])
              output += tokens[tokenIdx] + "[" + " ".join(tokenTags) + "][%s] " % (tokenoffsets[tokenCounter],) 
              tokenCounter += 1
      return output
           
def runELG(txt):
    with graph.as_default():
      sentences = SentenceSplit(txt)
      tokenoffsets = []
      offset = 0
      for sentenceIdx in range(len(sentences)):
          #print(sentences[sentenceIdx])
          tokens = sentences[sentenceIdx]['tokens']
          sentence = sentences[sentenceIdx]['text']
          #tokens = word_tokenize(sentence)
          tls = len(tokens)
          for ti, token in enumerate(tokens):
              if ti == 0 and not sentence.startswith(token): # this is to check for initial whitespaces
                  offset += sentence.find(token)
              tl = len(token)
              start, end = offset, offset+tl
              #checkOffsets(txt, token, start, end) # in place method to check and crash when indexing goes off (may want to comment this out when I'm positive this works in all cases)
              tokenoffsets.append((start, end))
              offset += tl
              if ti < tls-1:
                  # getting space in between current and next token here
                  offset += txt[end:].find(tokens[ti+1])

          if sentenceIdx < len(sentences)-1:
              # getting length of trailing stuff here
              offset += txt[offset:].find(sentences[sentenceIdx+1]['text'])
      # :: Load the model ::
      lstmModel = BiLSTM.loadModel("models/blstm-cnn-crf.h5")
      # :: Read input ::
      #sentences = [{'tokens': sent} for sent in sentences]
      addCharInformation(sentences)
      addCasingInformation(sentences)
      dataMatrix = createMatrices(sentences, lstmModel.mappings, True)
      # :: Tag the input ::
      tags = lstmModel.tagSentences(dataMatrix)
      tokenCounter = 0

      #addEntities(tokenoffsets,tags['LER'])
      # :: Output to stdout ::
      entities = []
      for sentenceIdx in range(len(sentences)):
          tokens = sentences[sentenceIdx]['tokens']
          ent = []
          for tokenIdx in range(len(tokens)):
              tokenTags = []
              #for modelName in sorted(tags.keys()):
              #    print(tags[modelName][sentenceIdx][tokenIdx])
              #    tokenTags.append(tags[modelName][sentenceIdx][tokenIdx])
              #output += tokens[tokenIdx] + "[" + " ".join(tokenTags) + "][%s] " % (tokenoffsets[tokenCounter],)
              tag = tags['LER'][sentenceIdx][tokenIdx]
              tag = re.sub('[BI]-', '', tag)
              print("DEBUG TAG: ",tag)
              if tag != 'O':
                  ent.append([tokenoffsets[tokenCounter], tag, tokens[tokenIdx]])
              if tag == 'O' and ent:
                  #print('Adding entity to List')
                  entities.append(ent)
                  ent = []
              tokenCounter += 1
          if ent:
              entities.append(ent)

      hardcoded_taClassRefMap = {  # may want to not hard-code this...
          'PER': 'http://dbpedia.org/ontology/Person',
          'RR': 'http://dbpedia.org/ontology/RR',
          'AN': 'http://dbpedia.org/ontology/AN',
          'LOC': 'http://dbpedia.org/ontology/Location',
          'LD': 'http://dbpedia.org/ontology/LD',
          'ST': 'http://dbpedia.org/ontology/ST',
          'STR': 'http://dbpedia.org/ontology/STR',
          'LDS': 'http://dbpedia.org/ontology/LDS',
          'ORG': 'http://dbpedia.org/ontology/Organisation',
          'UN': 'http://dbpedia.org/ontology/UN',
          'INN': 'http://dbpedia.org/ontology/INN',
          'GRT': 'http://dbpedia.org/ontology/GRT',
          'MRK': 'http://dbpedia.org/ontology/MRK',
          'NRM': 'http://dbpedia.org/ontology/NRM',
          'GS': 'http://dbpedia.org/ontology/GS',
          'VO': 'http://dbpedia.org/ontology/VO',
          'EUN': 'http://dbpedia.org/ontology/EUN',
          'REG': 'http://dbpedia.org/ontology/REG',
          'VS': 'http://dbpedia.org/ontology/ST',
          'VT': 'http://dbpedia.org/ontology/VT',
          'RS': 'http://dbpedia.org/ontology/RS',
          'LIT': 'http://dbpedia.org/ontology/LIT',
          'MISC': 'http://dbpedia.org/ontology/Miscellaneous'  # not sure if this one actually exists
      }
      ner_annotations = {'PER': [], 'RR': [], 'AN': [], 'LOC': [], 'LD': [], 'ST': [], 'STR': [], 'LDS': [], 'ORG': [],
                         'UN': [], 'INN': [], 'GRT': [], 'MRK': [], 'NRM': [], 'GS': [], 'VO': [], 'EUN': [], 'REG': [],
                         'VS': [], 'VT': [], 'RS': [], 'LIT': [], 'MISC': []}
      for ent in entities:
        tag = list(set([x[1] for x in ent]))[0]
        beginIndex = ent[0][0][0]
        endIndex = ent[-1][0][1]
        anchorOf = txt[beginIndex:endIndex]
        print("DEBUG TAG2: ", hardcoded_taClassRefMap[tag])
        annot_dict = {
          "start": beginIndex,
          "end": endIndex,
          "features": {
             "nif:anchorOf": anchorOf,
             "itsrdf:taClassRef": hardcoded_taClassRefMap[tag]
             #"itsrdf:taIdentRef": "dbo:Berlin"
          }
        }
        ner_annotations[tag].append(annot_dict)
      #nifmodel.addEntities(outputlabels)
      return ner_annotations


def runNIF(nifDocument,outformat="turtle"): 
    txt = nifDocument.context.nif__is_string
    with graph.as_default():
      sentences = SentenceSplit(txt)
      tokenoffsets = []
      offset = 0
      # sentences = sent_tokenize(isstr) # make sure that this is in line with whatever the backend (ner.py in my original case) is doing to process the entire text (i.e. this may use another procedure for sentence splitting or tokenisation)
      #for si, sentence in enumerate(sentences):
      for sentenceIdx in range(len(sentences)):
          print(sentences[sentenceIdx])
          tokens = sentences[sentenceIdx]['tokens']
          sentence = sentences[sentenceIdx]['text']
          #tokens = word_tokenize(sentence)
          tls = len(tokens)
          for ti, token in enumerate(tokens):
              if ti == 0 and not sentence.startswith(token): # this is to check for initial whitespaces
                  offset += sentence.find(token)
              tl = len(token)
              start, end = offset, offset+tl
              #checkOffsets(txt, token, start, end) # in place method to check and crash when indexing goes off (may want to comment this out when I'm positive this works in all cases)
              tokenoffsets.append((start, end))
              offset += tl
              if ti < tls-1:
                  # getting space in between current and next token here
                  offset += txt[end:].find(tokens[ti+1])

          if sentenceIdx < len(sentences)-1:
              # getting length of trailing stuff here
              offset += txt[offset:].find(sentences[sentenceIdx+1]['text'])
      # :: Load the model ::
      lstmModel = BiLSTM.loadModel("models/blstm-cnn-crf.h5")
      # :: Read input ::
      #sentences = [{'tokens': sent} for sent in sentences]
      addCharInformation(sentences)
      addCasingInformation(sentences)
      dataMatrix = createMatrices(sentences, lstmModel.mappings, True)
      # :: Tag the input ::
      tags = lstmModel.tagSentences(dataMatrix)
      tokenCounter = 0

      #addEntities(tokenoffsets,tags['LER'])
      # :: Output to stdout ::
      entities = []
      for sentenceIdx in range(len(sentences)):
          tokens = sentences[sentenceIdx]['tokens']
          ent = []
          for tokenIdx in range(len(tokens)):
              tokenTags = []
              #for modelName in sorted(tags.keys()):
              #    print(tags[modelName][sentenceIdx][tokenIdx])
              #    tokenTags.append(tags[modelName][sentenceIdx][tokenIdx])
              #output += tokens[tokenIdx] + "[" + " ".join(tokenTags) + "][%s] " % (tokenoffsets[tokenCounter],) 
              tag = tags['LER'][sentenceIdx][tokenIdx]
              tag = re.sub('[BI]-', '', tag)
              #print(tag)
              if tag != 'O':
                  ent.append([tokenoffsets[tokenCounter], tag, tokens[tokenIdx]])
              if tag == 'O' and ent:
                  #print('Adding entity to List')
                  entities.append(ent)
                  ent = []
              tokenCounter += 1
          if ent:
              entities.append(ent)

      hardcoded_taClassRefMap = {  # may want to not hard-code this...
          'PER': 'http://dbpedia.org/ontology/Person',
          'RR': 'http://dbpedia.org/ontology/RR',
          'AN': 'http://dbpedia.org/ontology/AN',
          'LOC': 'http://dbpedia.org/ontology/Location',
          'LD': 'http://dbpedia.org/ontology/LD',
          'ST': 'http://dbpedia.org/ontology/ST',
          'STR': 'http://dbpedia.org/ontology/STR',
          'LDS': 'http://dbpedia.org/ontology/LDS',
          'ORG': 'http://dbpedia.org/ontology/Organisation',
          'UN': 'http://dbpedia.org/ontology/UN',
          'INN': 'http://dbpedia.org/ontology/INN',
          'GRT': 'http://dbpedia.org/ontology/GRT',
          'MRK': 'http://dbpedia.org/ontology/MRK',
          'NRM': 'http://dbpedia.org/ontology/NRM',
          'GS': 'http://dbpedia.org/ontology/GS',
          'VO': 'http://dbpedia.org/ontology/VO',
          'EUN': 'http://dbpedia.org/ontology/EUN',
          'REG': 'http://dbpedia.org/ontology/REG',
          'VS': 'http://dbpedia.org/ontology/ST',
          'VT': 'http://dbpedia.org/ontology/VT',
          'RS': 'http://dbpedia.org/ontology/RS',
          'LIT': 'http://dbpedia.org/ontology/LIT',
          'MISC': 'http://dbpedia.org/ontology/Miscellaneous'  # not sure if this one actually exists
      }

      SUPPORTED_NIFFORMATS = ['turtle', 'xml', 'json-ld', 'conll', 'json']

      if outformat in SUPPORTED_NIFFORMATS:
          pass
      else:
          sys.stderr.write('ERROR: "turtle" and "CONLL" are the only supported output formats for now. Returning None, will probably crash afterwards...\n')
          return None

      if outformat == 'turtle':
          for ent in entities:
              tag = list(set([x[1] for x in ent]))[0]
              beginIndex = ent[0][0][0]
              endIndex = ent[-1][0][1]
              ee = NIFExtractedEntity(
                  reference_context=nifDocument.context,
                  begin_end_index=(beginIndex, endIndex),
                  anchor_of=txt[beginIndex:endIndex],
                  entity_uri=hardcoded_taClassRefMap[tag])
              nifDocument.add_extracted_entity(ee)
          return nifDocument
      elif outformat == 'conll':
          output = ""
          tokenCounter = 0

          # :: Output to stdout ::
          for sentenceIdx in range(len(sentences)):
              tokens = sentences[sentenceIdx]['tokens']

              for tokenIdx in range(len(tokens)):
                  tokenTags = []
                  for modelName in sorted(tags.keys()):
                      tokenTags.append(tags[modelName][sentenceIdx][tokenIdx])
                  output += tokens[tokenIdx] + "[" + " ".join(tokenTags) + "][%s] " % (tokenoffsets[tokenCounter],)
                  tokenCounter += 1
          return output
      elif outformat == 'json':
          output = ""
          tokenCounter = 0

          # :: Output to stdout ::
          for sentenceIdx in range(len(sentences)):
              tokens = sentences[sentenceIdx]['tokens']

              for tokenIdx in range(len(tokens)):
                  tokenTags = []
                  for modelName in sorted(tags.keys()):
                      tokenTags.append(tags[modelName][sentenceIdx][tokenIdx])
                  output += tokens[tokenIdx] + "[" + " ".join(tokenTags) + "][%s] " % (tokenoffsets[tokenCounter],)
                  tokenCounter += 1
          return output
          
 
def addEntities(tokenoffsets,labels):
    print(labels)
    #assert len(tokenoffsets) == len(labels), "ERROR: lengths of output labels (%i) and token indices (%i) do not match" % (len(labels), len(tokenoffsets))
    ## deciding to join together consecutive labels of the same type here. Since I haven't really seen any tags starting with B-, only I- ones (also entity-initially), feel that I can't really trust the BIO scheme for proper MWU entity handling

    entities = []
    zipiter = iter(zip(tokenoffsets, labels))
    index = 0
    for tupl in zipiter:
        indices, labeldict = tupl[0], tupl[1]
        skip = 0
        print("LABELDICT: ", labeldict)
        if labeldict['tag'] in entityLabelVocab:
            ent = []
            tag = re.sub('[BI]-', '', labeldict['tag'])
            ent.append([indices, tag, labeldict['word']])
            n = min(len(labels)-1, index+1)
            if not n == index:
                while re.sub('[BI]-', '', labels[n]['tag']) == tag:
                    ent.append([tokenoffsets[n], tag, labels[n]['word']])
                    n += 1
                    skip += 1
                    if n == len(labels):
                        break
            entities.append(ent)
        index += 1
        for i in range(skip):
            index += 1
            next(zipiter)


if __name__ == "__main__":
    print(run(""))
