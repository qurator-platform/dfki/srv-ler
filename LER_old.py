#!/usr/bin/python
# This scripts loads a pretrained model and a raw .txt files. It then performs sentence splitting and tokenization and passes
# the input sentences to the model for tagging. Prints the tokens and the tags in a CoNLL format to stdout
# Usage: python RunModel.py modelPath inputPath
# For pretrained models see docs/Pretrained_Models.md

from __future__ import print_function
from somajo import Tokenizer, SentenceSplitter
from pynif import NIFCollection
from util.preprocessing import addCharInformation, createMatrices, addCasingInformation
from neuralnets.BiLSTM import BiLSTM
import sys
import tensorflow as tf

graph = tf.get_default_graph()


# :: Tokenize German text ::
def SentenceSplit(text):

    tokenizer = Tokenizer(split_camel_case=False, token_classes=False, extra_info=False)
    tokens = tokenizer.tokenize(text)

    sentence_splitter = SentenceSplitter(is_tuple=False)
    sentences = sentence_splitter.split(tokens)
    return sentences

# :: Read the input ::
def ReadNIF(inputPath):
    with open(inputPath, 'r', encoding='utf8') as f: text = f.read()
    parsed_collection = NIFCollection.loads(text, format='turtle')
    if len(parsed_collection.contexts) == True:
        sentences = SentenceSplit(parsed_collection.contexts[0].mention)
        sentences = [{'tokens': sent} for sent in sentences]
        return sentences
    else: 
        print("Error: More mentions in NIF than one ")
        return []

def ReadTXT(inputPath):
    with open(inputPath, 'r', encoding='utf8') as f: text = f.read()
    sentences = SentenceSplit(text)
    sentences = [{'tokens': sent} for sent in sentences]
    return sentences
    

def run(inpt): 

    with graph.as_default():
    
      output = ""
      txt = "Am 7. Maerz 2006 traf sich die saarlaendische Landesregierung unter Vorsitz des Ministerpraesidenten Mueller mit Vertretern der Evangelischen Kirche."  
      # input some sentence
      if len(inpt) > 0:
          txt = inpt   
  
      # :: Load the model ::
      lstmModel = BiLSTM.loadModel("models/blstm-cnn-crf.h5")
  
      # :: Read input ::
      sentences = SentenceSplit(txt)
      sentences = [{'tokens': sent} for sent in sentences]
      addCharInformation(sentences)
      addCasingInformation(sentences)
      dataMatrix = createMatrices(sentences, lstmModel.mappings, True)
  
      # :: Tag the input ::
      tags = lstmModel.tagSentences(dataMatrix)
  
      # :: Output to stdout ::
      for sentenceIdx in range(len(sentences)):
          tokens = sentences[sentenceIdx]['tokens']
  
          for tokenIdx in range(len(tokens)):
              tokenTags = []
              for modelName in sorted(tags.keys()):
                  tokenTags.append(tags[modelName][sentenceIdx][tokenIdx])
              output += tokens[tokenIdx] + "[" + " ".join(tokenTags) + "]" + " " 
      
      return output
           


if __name__ == "__main__":
    print(run(""))
