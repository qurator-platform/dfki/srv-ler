# Legal Entity Recognition Suite by Elena Leitner based on emnlp2017-blstm-cnn-crf

This is a service repository that provides [Legal ER](https://github.com/elenanereiss/Legal-Entity-Recognition) based on UKPLab's NER tool from the repository [emnlp2017-blstm-cnn-crf](https://github.com/UKPLab/emnlp2017-bilstm-cnn-crf). It mostly followqs the repository structure of mnlp2017-blstm-cnn-crf. 

Please also see the publication: 
Reimers, Nils ; Gurevych, Iryna (2017). Reporting Score Distributions Makes a Difference: Performance Study of LSTM-networks for Sequence Tagging. In: Proceedings of the 2017 Conference on Empirical Methods in Natural Language Processing (EMNLP), Copenhagen, Denmark


