#!/usr/bin/python3
from flask import Flask, Response, flash, request, redirect, url_for

from nif.annotation import *

from flask_cors import CORS
import os
import json
import shutil
from werkzeug.utils import secure_filename
import zipfile

import service

"""

then to start run:
export FLASK_APP=flaskController.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run
example calls:
curl -X GET localhost:5000/welcome
elg test call:
curl -X POST localhost:5000/ -H 'Content-Type: application/json' -d '{}'
"""

app = Flask(__name__)
app.secret_key = "super secret key"
CORS(app)

@app.route('/eLer/analyzeText', methods=['POST'])
def analyze():
    if request.method == 'POST':
        cType = request.headers["Content-Type"]
        accept = request.headers["Accept"]
        data=request.stream.read().decode("utf-8")
        if accept == 'text/turtle':
            pass
        elif accept == 'application/json-ld':
            pass
        else:
            return 'ERROR: the Accept header '+accept+' is not supported!'
        if cType == 'text/plain':
            uri_prefix="http://lynx-project.eu/res/"+str(uuid.uuid4())
            d = NIFDocument.from_text(data, uri_prefix)
        elif cType == 'text/turtle':
            d = NIFDocument.parse_rdf(data, format='turtle')
        else:
            return 'ERROR: the contentType header '+cType+' is not supported!'

        annotatedNIF = service.analyzeNIF(d)
        if accept == 'text/turtle':
            return annotatedNIF.serialize(format="ttl")
        elif accept == 'application/json-ld':
            return annotatedNIF.serialize(format="json-ld")
        return annotatedNIF.serialize(format="ttl")
    else:
        return 'ERROR, only POST method allowed.'


def create_error(code, message):
    failure_response_dict = {
        "failure": {
            "errors": [{
                "code": code,
                "text": message
            }]
        }
    }
    return Response(response=json.dumps(failure_response_dict), status=500, content_type='application/json')


##################### entity spotting #####################
@app.route('/', methods=['POST'])
def spotEntitiesELG():
    if request.method == 'POST':
        ctype = request.headers["Content-Type"]
        print("Content type: {}".format(ctype))
        accept = request.headers["Accept"]
        print("Response type: {}".format(accept))

        if not request.data:
            return create_error("elg.request.missing",
                                "Error: No request provided in message")

        request_body = request.data.decode('utf-8')
        request_dict = json.loads(request_body)

        mimetype = request_dict['mimeType']
        if mimetype == 'text/plain':
            content = request_dict['content']
            print("Content: {}".format(content))
        else:
            return create_error("elg.request.text.mimeType.unsupported",
                                "Error: MIME type {} not supported by this service".format(mimetype))

        if 'application/json' in accept:
            pass
        else:
            return create_error("elg.request.type.unsupported",
                                "ERROR: Request type {} not supported by this service".format(accept))

        if ctype == 'application/json':
            ner_annotations = service.analyzeELG(content)
            response_dict = {
                "response": {
                    "type": "texts",
                    "texts": [{
                        "content": content,
                        "annotations": {
                            "Person": ner_annotations['PER'],
                            "Judge": ner_annotations['RR'],
                            "Lawyer": ner_annotations['AN'],
                            "Location": ner_annotations['LOC'],
                            "Country": ner_annotations['LD'],
                            "City": ner_annotations['ST'],
                            "Street": ner_annotations['STR'],
                            "Landscape": ner_annotations['LDS'],
                            "Organisation": ner_annotations['ORG'],
                            "Company": ner_annotations['UN'],
                            "Institution": ner_annotations['INN'],
                            "Court": ner_annotations['GRT'],
                            "Brand": ner_annotations['MRK'],
                            "Law": ner_annotations['GS'],
                            "Ordinance": ner_annotations['VO'],
                            "EU legal norm": ner_annotations['EUN'],
                            "Legal norm": ner_annotations['NRM'],
                            "Regulation": ner_annotations['VS'],
                            "Contract": ner_annotations['VT'],
                            "Case-by-c. regul.": ner_annotations['REG'],
                            "Court decision": ner_annotations['RS'],
                            "Legal literature": ner_annotations['LIT'],
                            "Miscellaneous": ner_annotations['MISC']
                        }
                    }]
                }
            }
            return Response(response=json.dumps(response_dict), status=200, content_type='application/json')
        else:
            return create_error("elg.request.type.unsupported",
                                'Request type {} not supported by this service'.format(ctype))
    else:
        return create_error("elg.request.invalid",
                            'Invalid request message')


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8080))
    app.run(host='localhost', port=port, debug=True)
