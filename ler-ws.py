'''
Created on 23.08.2019

@author: mamo06
'''
import cherrypy
import LER


class LERWebService(object):
     
    def index(self):
        return ""
    
    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def process(self, input=None): # remove 2nd parameter for POST 
        
        if input == None:
            output = "Please specify an input parameter by appending '/?input=ANY_STRING_in_EN_D' to the URL in the browser."
        else: 
            # data = cherrypy.request.json # for POST
            output = {"result": LER.run(input)}
        
        #cherrypy.engine.restart() // mean hack, now fixed.         
        return output


    @cherrypy.expose
    def restart(self):
        cherrypy.engine.restart()
        
    index.exposed = True

if __name__ == '__main__':
    config = {'server.socket_host': '0.0.0.0'}
    cherrypy.config.update(config)
    cherrypy.quickstart(LERWebService())
    