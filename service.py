from nltk.corpus import stopwords
import string
import os
from nif.annotation import *

import LER

def analyzeNIF(nifDocument,outformat="turtle"):
    d = nifDocument
    text = d.context.nif__is_string


    #output = {"result": LER.run(text)}
    output = LER.runNIF(nifDocument,outformat)
    #print("RESULT: ", output)

    #ex_uri = 'http://example.com/index#some'
    #ee = NIFExtractedEntity(
    #      reference_context=d.context,
    #      begin_end_index=(0, 4),
    #      anchor_of=text[0:4],
    #      entity_uri=ex_uri)

    #d.add_extracted_entity(ee)
    #kwargs = {"nif__summary" : summary}    
    #nc = NIFContext(text,d.context.uri_prefix,**kwargs)
    #d2 = NIFDocument(nc, structures=d.structures)
    #return d
    return output

def analyzeELG(content):
    annotations = LER.runELG(content)
    return annotations

def main():

    samplenif = """
@prefix itsrsdf: <http://www.w3.org/2005/11/its/rdf#> .
@prefix nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<http://qurator.ai/documents/1566481330.803265#offset_0_127> a nif:Context,
        nif:String ;
    nif:beginIndex "0"^^xsd:nonNegativeInteger ;
    nif:endIndex "127"^^xsd:nonNegativeInteger ;
    nif:isString \"""
    Ich bin in Berlin, Julian ist in Madrid.

    Kommt dir Julian Moreno Schneider bekannt   vor?
    Und was mit Georg Rehm?\""" .
    """
    nifDocument = NIFDocument.parse_rdf(samplenif, format='turtle')

    d = analyzeNIF(nifDocument)
    print(d.serialize(format="ttl"))

if __name__ == "__main__":
    result = main()

